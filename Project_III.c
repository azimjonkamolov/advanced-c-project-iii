// Name: Advanced C project III
// Time: 01:06 12.06.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to store students' info by name, phone number, birth day, using dynamic memory and files
// This program has its README file


#include<stdio.h>   // THE HEADER FILE
#include<stdlib.h>  // THE HEADER FILE
#include<string.h>  // THE HEADER FILE

int i, t, n, num, owerflowcount; // global variables >> num for structure >> n for switch >> t for size of loops 

struct Student
{
	char *name;
	char *phone;
	char *bday;
	
}temp[30];

struct Student **lol;		// global declaration of structure, so it can be accessed from any function

void reg();
void dis();
void del();
void find();
void regfromfile(); // to read from a file 
void store(); // to store a filnal info into the file

int main()
{
	int flag=0;
	printf("Max_num:");
	scanf("%d", &num);
	lol=(struct Student**)malloc(num*sizeof(struct Student*)); // allocation of the size
	
	for(i=0;i<num;i++)
	{
		lol[i]=(struct Student*)malloc(sizeof(struct Student));
		lol[i]->name=(char*)malloc(101*sizeof(char));
		lol[i]->phone=(char*)malloc(101*sizeof(char));
		lol[i]->bday=(char*)malloc(101*sizeof(char));
	}
	i=0;
	while(1)
	{
		printf("*****Menu*****\n");
		printf("<1.Registration><2.ShowAll><3.Delete><4.FindByBirth><5.RegFromFile><6.Exit>\n");
		printf("Enter_the_menu_number:");
		scanf("%d", &n);

		switch(n)
		{
			case 1:
				owerflowcount+=1; 
				if(owerflowcount<=num)
				{
					flag=1;
					reg();
					break;	
				}
				else
				{
					printf("OVERFLOW\n");
					continue;	
				}
			case 2:
				dis();
				break;
			case 3:
				del();
				break;
			case 4:
				find();
				break;
			case 5:
				regfromfile();
				break;
			case 6:
				store();
				free(lol);
				exit(1);
		}
		if(flag==1)
		{
			t+=1;
		}
	}

}
void reg()
{
	int j;
	printf("Name:");
	scanf("%s", lol[i]->name);
	printf("Phone_number:");
	scanf("%s", lol[i]->phone);
	printf("Birth:");
	scanf("%s", lol[i]->bday);
	printf("<<%d>>\n", i+1);
	for (i=0;i<=t;i++)                   // TO STORE AS ALPHABET
	{
		for(j=0;j<t-i;j++)              // TO STORE AS ALPHABET
		{
			{
				if(strcmp(lol[j]->name,lol[j+1]->name)>0) // COMPARE STRINGS
				{		
					*temp=*lol[j];        // CHANGE THEM HERE
					*lol[j]=*lol[j+1];    // CHANGE THEM HERE
					*lol[j+1]=*temp;      // CHANGE THEM HERE
				}									
			}
		}
	}
}
void dis()                       // DISPLAY FUNCTION FOR ALPHABETIC ORDER
{
	int j, flag=0;
	if(lol[0]->bday!=0)			// if there is a value in side of the structure, then goes below
	{
		for(i=0;i<t;i++)                                        // TO SHOW THEM 
		{
			printf("%s ", lol[i]->name);   // TO SHOW THE NAME
			printf("%s ", lol[i]->phone);    // TO SHOW THE NUMBER
			printf("%s\n", lol[i]->bday);   // TO SHOW THE BIRTHDAY	
		}
	}
}

void del()
{
	char fname[20];
	int fnum, flag=0, l;
	if(i==0)
	{
		printf("NO MEMBER\n");
	}
	else
	{
		printf("Name:");
		scanf("%s", fname);
		for(i=0;i<t;i++)
		{
			if(strcmp(fname,lol[i]->name)==0)
			{
				flag=1;
				fnum=i;
				owerflowcount-=1;
				for(i=fnum;i<t;i++)
				{
					lol[i]=lol[i+1];	// to delete, front one replaces the chosen one
				}
				break;
			}
		}
		if(flag==1)
		{
			t-=1; // to reduce the general size
		}
	}
}

void find()                           
{
	int fbday, flag=0, len, chnum, result, j;
	char tnum[20];                                             
	printf("Birth:");
	scanf("%d", &fbday);
	for(i=0;i<t;i++)        // first to get 2 month digits                       
	{
		len=strlen(lol[i]->bday);
		for(j=0; j<len; j++)
		{
			result = result * 10 + ( lol[i]->bday[j] - '0' );
		}
		chnum=result/100;
		chnum=chnum%100; // up ot here
		if(fbday == chnum ) // if found, then goes inside the loop
		{
			printf("%s ", lol[i]->name);
			printf("%s ", lol[i]->phone);
			printf("%s\n", lol[i]->bday);
		}
		else
		{
			result = 0;	
		}	
	}
}

void store() // before free is used and before it goes to exit it stores all info into the file
{
	FILE *fp;
	fp=fopen("PHONE_BOOK.txt", "w");
	if(fp==NULL)
	{
		printf("file nope!\n");
	}
	for(i=0;i<t;i++)
	{
		fprintf(fp, "%s", lol[i]->name);
		fprintf(fp, " ");
		
		fprintf(fp, "%s", lol[i]->phone);
		fprintf(fp, " ");
	
		fprintf(fp, "%s", lol[i]->bday);
		fprintf(fp, "\n");		
	}
	fclose(fp);
}

void regfromfile() // to read a from file 
{
	FILE * fp;
	int nono=0;
	nono+=t; // to make correct, if user interts first and then calls this func it starts from where a user left
	char c;
	fp = fopen("PHONE_BOOK.txt", "r");
	char arr1[32], arr2[32], arr3[32], tempt; // emp arrays
	while (!feof(fp)) 
	{
		fgets(arr1, 32, fp);
		fgets(arr2, 32, fp);
		fgets(arr3, 32, fp);
		strcpy(lol[i]->name, arr1);
		strcpy(lol[i]->phone, arr2);
		strcpy(lol[i]->bday, arr3);	
	}
	t+=i; //then adds this value into t, so it also increases 
}







